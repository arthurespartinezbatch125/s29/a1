let express = require("express");
const PORT = 4000;
let web = express();
let userArrays = [];

web.use(express.json());
web.use(express.urlencoded({extended:true}))
//1.
web.get("/home", (req,res)=>res.send(`Welcome to the homepage`))
//2.
web.post("/signup", (req,res)=>{
	if(req.body.username.length !== 0 && req.body.password.length !== 0){
		res.send(`User ${req.body.username} succesfully registered`);

		let newUser = {"username": req.body.username};
		userArrays.push(newUser)
		return userArrays
	} else {
		res.send(`Please input BOTH username and password`)
	}
	
})
//3.

web.get("/users", (req,res)=>{
	res.json(userArrays)
});

web.listen(PORT, ()=>{
	console.log(`Server running at port ${PORT}`)
})